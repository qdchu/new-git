# Git 安装

> git官网：https://git-scm.com/
> 查看版本：git --version

# Git 常用命令

* git config --global user.name 用户名 -- 设置用户签名
* git config --global user.email 邮箱 -- 设置用户签名
* git init（int） -- 初始化本地状态
* git status -- 查看本地库状态
* git add -- 添加到暂存区
* git commit -m "日志信息" 文件名 -- 提交到本地库
* git reflog -- 查看历史记录
* git log -- 查看版本详细信息
* git reset --hard 版本号 -- 版本穿梭
* git rm 文件名 -- 删除文件，需要提交到本地库中（git commit）

## 1.设置签名

> git config --global user.name 用户名 -- 设置用户签名
> git config --global user.email 邮箱 -- 设置用户签名

## 2.初始化本地库

> git int

## 3.查看本地库状态

> git status

## 4.添加值占存区

> git add 文件
> git rm --cached 文件 删除占存区的文件

## 5.提交本地库

> git commit -m "日志信息" 文件名

## 6.版本穿梭

> git reset --hard 版本号

# Git 分支

## 分支操作

* git branch 分支别名 -- 创建分支
* git branch -v -- 查看分支
* git checkout 分支名 -- 切换分支
* git merge 分支名 -- 把指定的分支合并到当前分支上

## 1.查看分支

> git branch -v

## 2.创建分支

> git branch 分支别名

## 3.切换分支

> git checkout 分支名

## 4.合并分支

> git merge 分支名

## 5.分支冲突

# 团队协作
