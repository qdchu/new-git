# 配置 Git 忽略文件

1. 在 C:\Users\CH 创建 git.ignore
2. 在 .gitconfig 配置 core
    ~~~
        [core]
            excludesfile = C:/user/CH/git.ignore
    ~~~

# 在项目配置 Git 程序

> 设置->版本控制->Git

# VCC选项初始化本地库

# 关联 GitHub

1. 需要下载 GitHub 插件
2. 绑定账号
   > 设置 -> 版本控制 -> GitHub 