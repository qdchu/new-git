# 官网 https://github.com/

# 远程仓库操作

* git remote -v -- 查看当前所有远程地址别名
* git remote add 别名 远程地址 -- 起别名
* git push 别名 分支 -- 推送本地分支上的内容到远程仓库
* git clone 远程地址 -- 将远程仓库的内容克隆岛本地
* git pull 远程库地址别名 远程分支名 -- 将远程仓库对于分支最新的内容拉下来后与当前本地分支直接合并

# 流程

## 1.创建远程仓库别名

> git remote -v 查看当前所有远程地址别名
> git remote add 别名 远程地址

## 2.推送到本地分支到远程库

> git push 别名 分支

## 3.拉取到本地库

> git pull 别名 分支

## 4.克隆远程库到本地库

> git clone 远程地址

# SSH免密登录

> 需要生成 win的key，将key保存到代码仓库中